<?php

use Illuminate\Database\Seeder;

class EmpleadoTablaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Article::truncate();

        $faker= \Faker\Factory::Create();
        for ($i = 0; $i<50; $i++){
            Article::Create([
                'cedula' => $faker -> phoneNumber,
                'nombre' => $faker -> firstName,
                'apellido' => $faker -> lastName,
                'posicion' => $faker -> sentence,
                'fecha_ingreso' => $faker -> date,
                'salario' => $faker -> randomNumber,
                'gastos' => $faker -> randomNumber,
            ]);
        }
    }
}
