<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePagos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pagos', function (Blueprint $table) {
            $table->string('empleado_ced');
            $table->foreign('empleado_ced')
                ->references('cedula')->on('empleados')
                ->onDelete('cascade');
            $table->integer('cheque')->primary();
            $table->double('monto');
            $table->string('concepto_pago');
            $table->foreign('concepto_pago')
                ->references('concepto_pago')->on('c_pagos')
                ->onDelete('cascade');
            $table->date('fecha_pago');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pagos');
    }
}
