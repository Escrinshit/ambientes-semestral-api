FROM php:7.3.5-apache-stretch

MAINTAINER Carlos Tuñón


#COPY src/ /var/www/html
COPY site.conf /etc/apache2/sites-enabled/000-default.conf
RUN docker-php-ext-install -j$(nproc) pdo_mysql
RUN a2enmod rewrite headers 
