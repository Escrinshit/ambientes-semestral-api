#! /bin/bash
cd src
cp .env.example .env
php artisan key:generate
chmod 777 storage/framework -R
chmod 777 storage/logs -R
